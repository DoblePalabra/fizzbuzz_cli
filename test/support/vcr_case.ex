defmodule Test.Support.VCRCase do
  use ExUnit.CaseTemplate

  setup_all do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes", "fixture/custom_cassettes")
    :ok
  end
end
