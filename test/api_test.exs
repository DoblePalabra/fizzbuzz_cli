defmodule APITest do
  use Test.Support.VCRCase, async: true
  use ExVCR.Mock

  setup_all do
    API.Data.start_link([])
    API.Data.put_apiurl("http://localhost:3000")
  end

  test "gets the default list of numbers" do
    use_cassette "get_fizzbuzz" do
      {:ok, %{numbers: numbers}} = API.Fizzbuzz.index()

      numbers
      |> Enum.with_index()
      |> Enum.each(fn {num, index} -> assert num.value == index + 1 end)
    end
  end

  test "gets a page with a size" do
    use_cassette "get_fizzbuzz_pages" do
      {:ok, %{numbers: numbers}} = API.Fizzbuzz.index(3, 200)

      numbers
      |> Enum.with_index()
      |> Enum.each(fn {num, index} -> assert num.value == index + 601 end)
    end
  end

  test "returns error if it fails" do
    use_cassette "get_fizzbuzz_error" do
      :error = API.Fizzbuzz.index()
    end
  end

  test "logs in correctly" do
    use_cassette "login_ok" do
      {:ok, %{access_token: _token}} =
        API.Token.get(username: "test@test.com", password: "abcd1234")
    end
  end

  test "tries to login, but the user or password are invalid" do
    use_cassette "login_invalid" do
      :credentials_invalid = API.Token.get(username: "invalid@test.com", password: "abcd1234")
    end
  end

  test "tries to login, but server is down" do
    use_cassette "login_server_error" do
      :error = API.Token.get(username: "test@test.com", password: "abcd1234")
    end
  end

  test "registers correctly" do
    use_cassette "registration_ok" do
      {:ok, _} = API.Registration.register(username: "newuser@test.com", password: "abcd1234")

      {:ok, %{access_token: _token}} =
        API.Token.get(username: "newuser@test.com", password: "abcd1234")
    end
  end

  test "returns an error if the user already exists" do
    use_cassette "registration_user_already_exists" do
      :user_already_exists =
        API.Registration.register(username: "newuser@test.com", password: "abcd1234")
    end
  end

  test "returns an error if the server is down" do
    use_cassette "registration_server_error" do
      :error = API.Registration.register(username: "newuser@test.com", password: "abcd1234")
    end
  end

  test "returns the favs of a user" do
    use_cassette "fav_get" do
      {:ok, %{access_token: token}} =
        API.Token.get(username: "test@test.com", password: "abcd1234")

      API.Data.put_token(token)

      {:ok, %{numbers: favs}} = API.Fav.get()
      assert Enum.member?(favs, %{number: 3})
      assert Enum.member?(favs, %{number: 15})
      assert Enum.member?(favs, %{number: 18})
      API.Data.delete_token()
    end
  end

  test "GET /fav returns unauthorized" do
    use_cassette "fav_get_unauthorized" do
      :unauthorized = API.Fav.get()
    end
  end

  test "GET /fav returns error if the server is down" do
    use_cassette "fav_get_error" do
      :error = API.Fav.get()
    end
  end

  test "creates a new fav" do
    use_cassette "fav_post" do
      {:ok, %{access_token: token}} =
        API.Token.get(username: "test@test.com", password: "abcd1234")

      API.Data.put_token(token)

      {:ok, _} = API.Fav.fav(2)
      {:ok, %{numbers: favs}} = API.Fav.get()
      assert Enum.member?(favs, %{number: 2})
      API.Data.delete_token()
    end
  end

  test "POST /fav returns unauthorized" do
    use_cassette "fav_post_unauthorized" do
      :unauthorized = API.Fav.fav(2)
    end
  end

  test "POST /fav returns error if the server is down" do
    use_cassette "fav_post_error" do
      :error = API.Fav.fav(2)
    end
  end

  test "removes a fav" do
    use_cassette "fav_delete" do
      {:ok, %{access_token: token}} =
        API.Token.get(username: "test@test.com", password: "abcd1234")

      API.Data.put_token(token)

      {:ok, _} = API.Fav.unfav(2)
      {:ok, %{numbers: favs}} = API.Fav.get()
      refute Enum.member?(favs, %{number: 2})
      API.Data.delete_token()
    end
  end

  test "DELETE /fav returns unauthorized" do
    use_cassette "fav_delete_unauthorized" do
      :unauthorized = API.Fav.unfav(2)
    end
  end

  test "DELETE /fav returns error if the server is down" do
    use_cassette "fav_delete_error" do
      :error = API.Fav.unfav(2)
    end
  end
end
