defmodule FizzbuzzCliTest do
  use Test.Support.VCRCase, async: false
  use ExVCR.Mock

  import ExUnit.CaptureIO

  test "prints the usage instructions when run without --apiurl" do
    execute_main = fn ->
      FizzbuzzCli.main([])
    end

    assert capture_io([input: "quit\n"], execute_main) =~ "usage:"
  end

  test "prints the usage instructions when --apiurl is empty" do
    execute_main = fn ->
      FizzbuzzCli.main(["--apiurl"])
    end

    assert capture_io([input: "quit\n"], execute_main) =~ "usage:"
  end

  test "prints an error if the API server cannot be found" do
    execute_main = fn ->
      FizzbuzzCli.main(["--apiurl", "http://localhost:3000"])
    end

    use_cassette "server_not_running" do
      assert capture_io([input: "quit\n"], execute_main) =~
               "Couldn't connect to the API server. Is the server running?"
    end
  end

  test "prints a welcome message" do
    execute_main = fn ->
      FizzbuzzCli.main(["--apiurl", "http://localhost:3000"])
    end

    use_cassette "server_welcome" do
      assert capture_io([input: "quit\n"], execute_main) =~ "Welcome to FizzBuzz!"
    end
  end

  test "sets the api url in the agent" do
    execute_main = fn ->
      FizzbuzzCli.main(["--apiurl", "http://localhost:3000"])
    end

    use_cassette "server_welcome" do
      capture_io([input: "quit\n"], execute_main)
      assert API.Data.get_apiurl() == "http://localhost:3000"
    end
  end
end
