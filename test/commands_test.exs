defmodule CommandsTest do
  use Test.Support.VCRCase, async: false
  use ExVCR.Mock

  import ExUnit.CaptureIO

  defp execute_main do
    FizzbuzzCli.main(["--apiurl", "http://localhost:3000"])
  end

  defp capture(input) do
    capture_io([input: "#{input}\nquit\n"], &execute_main/0)
  end

  test "shows the help page" do
    use_cassette "command_help" do
      assert capture("help") =~ "Command list:"
    end
  end

  test "shows the help page if the command is not recognised" do
    use_cassette "command_help" do
      assert capture("what") =~ "Command list:"
    end
  end

  test "quits when the command is quit" do
    use_cassette "command_quit" do
      assert capture("quit") =~ "Bye!"
    end
  end

  test "lists a page of numbers" do
    use_cassette "command_page" do
      captured = capture("page 3 200")
      assert captured =~ "601"
      assert captured =~ "603 Fizz!"
      assert captured =~ "605 Buzz!"
      assert captured =~ "615 Fizzbuzz!"
      assert captured =~ "800"
    end
  end

  test "lists a page of numbers without parameters" do
    use_cassette "command_page_1" do
      captured = capture("page")
      assert captured =~ "1"
      assert captured =~ "100"
    end
  end

  test "lists a page of numbers with default size" do
    use_cassette "command_page_2" do
      captured = capture("page 1")
      assert captured =~ "101"
      assert captured =~ "200"
    end
  end

  test "logins correctly" do
    use_cassette "command_login_ok" do
      assert capture("login test@test.com abcd1234") =~ "Welcome, test@test.com!"
    end
  end

  test "logins incorrectly" do
    use_cassette "command_login_invalid" do
      assert capture("login invalid@test.com abcd1234") =~ "Username or password incorrect!"
    end
  end

  test "after logging in, page shows favourites" do
    use_cassette "command_page_after_login" do
      assert capture("login test@test.com abcd1234\n page") =~ "☆ 3 Fizz!"
    end
  end

  test "fav without being logged in returns a message" do
    use_cassette "command_fav_unauthed" do
      assert capture("fav 1") =~ "You need to log in first."
    end
  end

  test "fav a number" do
    use_cassette "command_fav" do
      captured = capture("login test@test.com abcd1234\nfav 1\npage")
      assert captured =~ "Ok!"
      assert captured =~ "☆ 1"
    end
  end

  test "unfav a number" do
    use_cassette "command_unfav" do
      assert capture("login test@test.com abcd1234\nunfav 3\npage") =~ "Ok!"
    end
  end
end
