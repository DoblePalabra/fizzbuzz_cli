# FizzbuzzCli

Connects to FizzBuzz API and offers a command line to test it.

## Requirements

* Elixir 1.6

## Usage

* Run `mix deps.get`
* Run `mix escript.build`
    * This will build a binary `fizzbuzz_cli` in the same folder
* Run `./fizzbuzz_cli` to see usage instructions.

## Tests

* Run `mix test`

