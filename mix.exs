defmodule FizzbuzzCli.MixProject do
  use Mix.Project

  def project do
    [
      app: :fizzbuzz_cli,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      escript: escript(),
      deps: deps(),
      elixirc_paths: ["lib", "test/support"]
    ]
  end

  def escript do
    [main_module: FizzbuzzCli]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:httpotion],
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpotion, "~> 3.0.2"},
      {:poison, "~> 3.1"},
      {:exvcr, "~> 0.8", only: :test}
    ]
  end
end
