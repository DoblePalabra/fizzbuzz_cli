defmodule API.Registration do
  import API.Base

  def register(username: username, password: password) do
    payload = %{
      user: %{
        email: username,
        password: password,
        password_confirmation: password
      }
    }

    case response = post("/registration", payload) do
      :invalid_entity ->
        :user_already_exists

      _ ->
        response
    end
  end
end
