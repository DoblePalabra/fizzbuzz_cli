defmodule API.Fizzbuzz do
  import API.Base

  def index(page \\ 0, size \\ 0) do
    # Interpolating like this is insecure
    get("/fizzbuzz?page=#{page}&per_page=#{size}")
  end
end
