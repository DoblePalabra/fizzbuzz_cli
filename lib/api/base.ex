defmodule API.Base do
  def get(path) do
    case HTTPotion.get(full_url(path), headers: headers()) do
      %HTTPotion.Response{status_code: code, body: body} when code in 200..299 ->
        {:ok, Poison.decode!(body, keys: :atoms)}

      %HTTPotion.Response{status_code: 401} ->
        :unauthorized

      _ ->
        :error
    end
  end

  def post(path, body) do
    case HTTPotion.post(
           full_url(path),
           body: Poison.encode!(body),
           headers: headers()
         ) do
      %HTTPotion.Response{status_code: code, body: body} when code in 200..299 ->
        {:ok, Poison.decode!(body, keys: :atoms)}

      %HTTPotion.Response{status_code: 401} ->
        :unauthorized

      %HTTPotion.Response{status_code: 422} ->
        :invalid_entity

      _ ->
        :error
    end
  end

  def delete(path, body) do
    case HTTPotion.delete(
           full_url(path),
           body: Poison.encode!(body),
           headers: headers()
         ) do
      %HTTPotion.Response{status_code: code, body: body} when code in 200..299 ->
        {:ok, Poison.decode!(body, keys: :atoms)}

      %HTTPotion.Response{status_code: 401} ->
        :unauthorized

      %HTTPotion.Response{status_code: 422} ->
        :invalid_entity

      _ ->
        :error
    end
  end

  defp full_url(path) do
    URI.merge(API.Data.get_apiurl(), path)
  end

  defp headers do
    [
      "Content-Type": "application/json",
      Authorization: "Bearer #{API.Data.get_token()}"
    ]
  end
end
