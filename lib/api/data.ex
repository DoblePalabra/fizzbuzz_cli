defmodule API.Data do
  use Agent

  def start_link(_opts) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def get_apiurl do
    Agent.get(__MODULE__, &Map.get(&1, :apiurl))
  end

  def put_apiurl(apiurl) do
    Agent.update(__MODULE__, &Map.put(&1, :apiurl, apiurl))
  end

  def get_token do
    Agent.get(__MODULE__, &Map.get(&1, :token))
  end

  def put_token(token) do
    Agent.update(__MODULE__, &Map.put(&1, :token, token))
  end

  def delete_token do
    Agent.update(__MODULE__, &Map.delete(&1, :token))
    Agent.update(__MODULE__, &Map.delete(&1, :favs))
  end

  def get_favs do
    Agent.get(__MODULE__, &Map.get(&1, :favs))
  end

  def put_favs(favs) do
    Agent.update(__MODULE__, &Map.put(&1, :favs, favs))
  end
end
