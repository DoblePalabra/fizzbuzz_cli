defmodule API.Token do
  import API.Base

  def get(username: username, password: password) do
    payload = %{
      grant_type: "password",
      username: username,
      password: password
    }

    case response = post("/oauth/token", payload) do
      :unauthorized ->
        :credentials_invalid

      {:ok, body} ->
        {:ok, body}

      _ ->
        response
    end
  end
end
