defmodule API.Fav do
  import API.Base

  def get() do
    get("/fav")
  end

  def fav(number) do
    body = %{number: number}
    post("/fav", body)
  end

  def unfav(number) do
    body = %{number: number}
    delete("/fav", body)
  end
end
