defmodule Command.Quit do
  def execute() do
    IO.puts("Bye!")
    :quit
  end
end
