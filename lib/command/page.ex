defmodule Command.Page do
  def execute([]), do: execute([0, 100])
  def execute([page]), do: execute([page, 100])

  def execute([page, per_page]) do
    {:ok, %{numbers: numbers}} = API.Fizzbuzz.index(page, per_page)

    numbers
    |> Enum.each(&print_number/1)

    :ok
  end

  defp print_number(number) do
    IO.puts("#{fav(number.value)}#{number.value} #{fizzbuzz(number)}")
  end

  defp fav(number) do
    favs = API.Data.get_favs()

    cond do
      favs == nil -> ""
      Enum.member?(favs, %{number: number}) -> "☆ "
      true -> ""
    end
  end

  defp fizzbuzz(%{fizz: false, buzz: false}), do: ""
  defp fizzbuzz(%{fizz: true, buzz: false}), do: "Fizz!"
  defp fizzbuzz(%{fizz: false, buzz: true}), do: "Buzz!"
  defp fizzbuzz(%{fizz: true, buzz: true}), do: "Fizzbuzz!"
end
