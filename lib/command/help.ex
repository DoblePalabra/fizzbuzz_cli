defmodule Command.Help do
  @help """
    Command list:
    help -> Show this page
    page [page_number [page_size]] -> Show a page of numbers
    login username password -> Logs in as username
    fav number -> Adds a number to favourites
    unfav number -> Removes a number from favourites
    quit -> Quit FizzBuzzCli
  """

  def execute() do
    IO.puts(@help)
    :ok
  end
end
