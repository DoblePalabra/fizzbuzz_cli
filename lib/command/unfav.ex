defmodule Command.Unfav do
  def execute([number]) do
    case API.Fav.unfav(number) do
      :unauthorized ->
        IO.puts("You need to log in first.")

      {:ok, _} ->
        IO.puts("Ok!")
        {:ok, %{numbers: favs}} = API.Fav.get()
        API.Data.put_favs(favs)

      _ ->
        IO.puts("Ok!")
    end

    :ok
  end

  def execute(_) do
    IO.puts("Usage: unfav number")
  end
end
