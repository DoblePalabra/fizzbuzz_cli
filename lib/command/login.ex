defmodule Command.Login do
  def execute([username, password]) do
    case API.Token.get(username: username, password: password) do
      {:ok, %{access_token: token}} ->
        API.Data.put_token(token)
        {:ok, %{numbers: favs}} = API.Fav.get()
        API.Data.put_favs(favs)

        IO.puts("Welcome, #{username}!")

      _ ->
        IO.puts("Username or password incorrect!")
    end
  end

  def execute(_) do
    IO.puts("Usage: login username password")
  end
end
