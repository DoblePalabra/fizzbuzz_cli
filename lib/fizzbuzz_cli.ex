defmodule FizzbuzzCli do
  @moduledoc """
  Main module of the app. Runs the process and works with parameters.
  """

  def main(args) do
    FizzbuzzCli.Supervisor.start_link([])
    parse_args(args)
    CommandParser.command_loop()
  end

  defp parse_args(args) do
    case OptionParser.parse(args) do
      {[apiurl: true], _, _} ->
        show_usage()

      {[apiurl: apiurl], _, _} ->
        API.Data.put_apiurl(apiurl)
        welcome(apiurl)

      _ ->
        show_usage()
    end
  end

  defp show_usage do
    IO.puts("usage: fizzbuzz_cli --apiurl http://localhost:3000")
  end

  defp welcome(url) do
    case HTTPotion.get(url) do
      %HTTPotion.ErrorResponse{} ->
        IO.puts("Couldn't connect to the API server. Is the server running?")

      %HTTPotion.Response{} ->
        IO.puts("Welcome to FizzBuzz!")
    end
  end
end
