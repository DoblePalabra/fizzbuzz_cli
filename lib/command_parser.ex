defmodule CommandParser do
  def command_loop() do
    result =
      case read_command() do
        ["help"] ->
          Command.Help.execute()

        ["quit"] ->
          Command.Quit.execute()

        ["page" | params] ->
          Command.Page.execute(params)

        ["login" | params] ->
          Command.Login.execute(params)

        ["fav" | params] ->
          Command.Fav.execute(params)

        ["unfav" | params] ->
          Command.Unfav.execute(params)

        _ ->
          Command.Help.execute()
      end

    if result != :quit do
      command_loop()
    end
  end

  defp read_command() do
    "FizzBuzz> "
    |> IO.gets()
    |> clean_command
  end

  defp clean_command(:eof) do
    nil
  end

  defp clean_command(data) do
    data
    |> String.trim()
    |> String.split()
  end
end
